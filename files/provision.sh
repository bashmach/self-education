FILE=/home/vagrant/.provision

if [ ! -f $FILE ];
then
    apt-get update
    apt-get install vim mc htop curl -y

    npm install -g ivm pm2
    ivm stable

    chown vagrant:vagrant /home/vagrant/.npm -R

    date >> /home/vagrant/.provision
fi

pm2 start /var/www/index.js --watch --node-args="--es_staging"

echo "Provision finished";
