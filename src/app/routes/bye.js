'use strict';

const Route = require('../../lib/route');

class ByeRoute extends Route {

  constructor(request) {
    super(request);

    this.name = 'bye';
    this.template = 'bye';
    this.viewParams.set('title', 'Bye - ' + this.viewParams.get('title'));

    this.patterns = new Set();
    this.patterns.add('/bye');
    this.patterns.add('/bye/:name/hahaha');

  }
}


module.exports = ByeRoute;