'use strict';

const Route = require('../../lib/route');

class HelloRoute extends Route {

  constructor(request) {
    super(request);

    this.name = 'hello';
    this.template = 'hello';

    //default name param just for fun
    this.viewParams.set('name', this.viewParams.get('title'));
    this.viewParams.set('title', 'Hello - ' + this.viewParams.get('title'));

    this.patterns = new Set();
    this.patterns.add('/');
    this.patterns.add('/hello');
    this.patterns.add('/hello/:name');
    this.patterns.add('/hello/:name/world');
  }
}


module.exports = HelloRoute;