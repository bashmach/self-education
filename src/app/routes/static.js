'use strict';

const fs = require('fs')
  , url = require('url')
  , mime = require('mime')
  , Route = require('../../lib/route');

class StaticRoute extends Route {

  constructor(request) {
    super(request);

    this.name = 'static';

    this.patterns = new Set();
    this.patterns.add(new RegExp("\\.(gif|jpg|jpeg|tiff|png|css|js|ico)$", "i"));

  }

  dispatch() {
    const self = this
      , filepath = __dirname + '/../../public/' + url.parse(self.request.url).pathname

    return new Promise(function (resolve, reject) {
      fs.stat(filepath, function (err, stat) {
        let stream, mimetype;

        if (err) {
          return reject(err);
        }

        mimetype = mime.lookup(filepath);

        self.view.response.writeHead(200, {
          "Content-Type": mimetype,
          'Content-Length': stat.size
        });

        stream = fs.createReadStream(filepath, 'binary');
        stream.pipe(self.view.response);

        stream.on('end', function () {
          resolve();
        });
      });
    });
  }
}

module.exports = StaticRoute;