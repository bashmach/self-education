const http = require("http")
  , Router = require('./lib/router');

http.createServer(function (request, response) {

  const router = new Router(request, response);
  router.dispatch();

}).listen(1337);
