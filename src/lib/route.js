'use strict';

const url = require('url')
  , querystring = require("querystring")
  , haiku = require('./haiku');

class Route {

  constructor(request) {
    this.viewParams = new Map();
    this.currentPattern = false;
    this.request = request;
    this.viewParams.set('haiku', haiku());
    this.viewParams.set('title', this.viewParams.get('haiku'));
  }

  matches(uri) {
    // parsed url
    const parsed = url.parse(uri)
      , self = this;

    let matches = false;

    // full pattern match
    if (this.patterns.has(parsed.pathname)) {
      matches = true;
      self.currentPattern = parsed.pathname;
    }

    // if full pattern match failed
    // go through the all route patterns
    if (!matches) {
      this.patterns.forEach(function(pattern) {
        // try to match with substitution of components
        if (typeof pattern === 'string') {
          const requestComponents = parsed.pathname.split('/')
            , patternComponents = pattern.split('/');

          if (patternComponents.length == requestComponents.length) {
            patternComponents.forEach(function(component, key) {
              // it's a param
              if (component.startsWith(':')) {
                requestComponents[key] = component;
              }
            });
;
            if (self.patterns.has(requestComponents.join('/'))) {
              matches = true;
              self.currentPattern = requestComponents.join('/');
            }
          }
        }

        // try to match regexp pattern
        if (pattern instanceof RegExp) {
          if (pattern.test(uri)) {
            matches = true;
            self.currentPattern = pattern.toString();
          }
        }

        if (matches) {
          return true;
        }
      });
    }

    return matches;
  }

  dispatch() {
    const self = this;

    return new Promise(function(resolve, reject) {
      self.view.render(self.template, self.params).then(
        function() {
          resolve();
        },
        function(error) {
          reject(error);
        }
      );
    });
  }

  get params() {
    const self = this
      , parsedUrl = url.parse(this.request.url)
      , requestComponents = parsedUrl.pathname.split('/')
      , patternComponents = this.currentPattern.split('/')
      , queryParams = querystring.parse(parsedUrl.query);

    // set query params to view
    for (var key in queryParams) {
      self.viewParams.set(key, queryParams[key]);
    }

    // set route params to view
    patternComponents.forEach(function (component, key) {
      // it's a param
      if (component.startsWith(':')) {
        self.viewParams.set(component.substring(1, component.length), requestComponents[key]);
      }
    });

    return self.viewParams;
  }
}

module.exports = Route;
