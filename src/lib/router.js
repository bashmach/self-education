'use strict';

const HelloRoute = require('../app/routes/hello')
  , ByeRoute = require('../app/routes/bye')
  , StaticRoute = require('../app/routes/static')
  , View = require('./view');

class Router {
  constructor(request, response) {
    this.request = request;
    this.view = new View(response);

    this.routes = [];
    this.routes.push(new HelloRoute(request));
    this.routes.push(new ByeRoute(request));
    this.routes.push(new StaticRoute(request));
  }

  error(msg) {
    this.view.response.writeHead(500, {"Content-Type": "text/plain"});
    this.view.response.write('Server error: '+msg);
    this.view.response.end();
  }

  match() {
    const uri = this.request.url;

    // matched route
    let matched;

    this.routes.forEach(function(route, key) {
      if (route.matches(uri)) {
        matched = route;

        return true;
      }
    })

    if (!matched) {
      return this.error('Route not found');
    }

    matched.view = this.view;

    return matched;
  }

  dispatch() {
    const route = this.match()
      , self = this;

    route.dispatch().then(
      function() {
        self.view.response.end();
      },
      function(err) {
        return self.error(err);
      }
    );
  }
}

module.exports = Router;
