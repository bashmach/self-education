'use strict';

const fs = require('fs')
  , path = require('path');

class View {
  constructor(response) {
    this.response = response;
  }

  render(template, params) {
    const templatePath = path.resolve(__dirname, '../app/views/'+template+'.html')
      , response = this.response;

    return new Promise(function(resolve, reject) {

      fs.stat(templatePath, function(err, stat) {
        if (err) {
          return reject('Template '+templatePath+' does not exists')
        }

        fs.readFile(templatePath, {'encoding': 'utf8'}, function(err, data) {
          const regex = new RegExp(/\${(.*?)}/);

          params.forEach(function (value, key) {
            const regex = new RegExp("\\$\{"+key+"\}", "g");
            data = data.replace(regex, value);
          });

          // if template still contains some params
          if (regex.test(data)) {
            reject('Unresolved variables in the template');
          } else {

            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(data);

            resolve();
          }
        });
      });
    });
  }
}

module.exports = View;